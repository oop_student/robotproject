/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.robotproject;

/**
 *
 * @author DELL
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0, 0, 7, 8, 100);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('S');
        System.out.println(robot);
        robot.walk('E', 2);
        System.out.println(robot);
        robot.walk(5);
        System.out.println(robot);
        robot.walk('S', 6);
        System.out.println(robot);
        robot.walk('S');
        System.out.println(robot);
        
    }
}
